import "./assets/scss/style.scss"

import Vue from 'vue'
import App from "./components/App.vue"
import router from "./router"
import {BootstrapVue, BootstrapVueIcons} from "bootstrap-vue"
import {ValidationObserver, ValidationProvider} from 'vee-validate'
import axios from 'axios'
import store from './store'


Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)


Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)
// Rules
require('./rules');


axios.defaults.baseURL = '/api/home'
axios.defaults.withCredentials = true;

axios.get('/sanctum/csrf-cookie', {baseURL: '/'}).then(response => {
  store.dispatch('get_user').then(launch)
})

function launch() {
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
}
