import whyItem from "./components/partials/why-item.vue"
import descriptionItem from "./components/partials/description-item.vue"
import testimonialItem from "./components/partials/testimonial-item.vue"
import pricingItem from "./components/partials/pricing-item.vue"
import YtEmbed from "./components/partials/yt-embed.vue"
import PromoBanner from "./components/partials/promo-banner.vue"
import FooterContent from "./components/partials/footer.vue"
import Trusted from "./components/partials/trusted.vue"

const homeComp = {
    whyItem,
    descriptionItem,
    testimonialItem,
    pricingItem,
    YtEmbed,
    FooterContent,
    PromoBanner,
    Trusted
}


export default homeComp;
