/*=========================================================================================
  File Name: index.js
  Description: Vuex store
==========================================================================================*/

import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios";

Vue.use(Vuex)

export default new Vuex.Store({
  getters: {
    user: state => state.user,
  },
  mutations: {
    set_user: (state, payload) => {
      state.user = payload
    }
  },
  state: {
    user: null,
  },
  actions: {
    get_user({commit}) {
      return axios.get('/user', {baseURL: '/api'})
        .then(({data}) => {
          commit('set_user', data)
          return Promise.resolve(data)
        })
    }
  },
  strict: process.env.NODE_ENV !== 'production',
  modules: {}
})
