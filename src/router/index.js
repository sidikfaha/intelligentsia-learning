import Vue from 'vue'
import Router from 'vue-router'
import Homepage from '../components/pages/home.vue'
import Programpage from '../components/pages/programme.vue'
import RegisterForm from '../components/pages/register.vue'
import Cours from '../components/pages/sub-program.vue'
import Chapter from '../components/pages/chapter.vue'
import Exam from '../components/pages/exam.vue'
import ExamSub from '../components/pages/exam-sub-program.vue'
import Discover from '../components/pages/dicover.vue'
import Register_1 from '../components/pages/register-1.vue'
import Partnership from '../components/pages/partnership.vue'

Vue.use(Router)

export default new Router({
  base: '/home',
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Homepage
    },
    {
      path: '/cours/:classe',
      name: 'programs',
      component: Programpage,
    },
    {
      path: '/cours/:classe/:cours',
      name: 'cours',
      component: Cours
    },
    {
      path: '/inscription',
      name: 'register',
      component: RegisterForm
    },
    {
      path: '/cours/:classe/:cours/:chapitre',
      name: 'chapter',
      component: Chapter
    },
    {
      path: '/examen/:exam',
      name: 'exam',
      component: Exam
    },
    {
      path: '/examen/bac/:cours',
      name: 'exam-sub',
      component: ExamSub
    },
    {
      path: '/discover',
      name: 'discover',
      component: Discover
    },
    {
      path: '/register-1',
      name: 'register-1',
      component: Register_1
    },
    {
      path: '/ecole-partenaire',
      name: 'partnership',
      component: Partnership
    },
  ]
})
